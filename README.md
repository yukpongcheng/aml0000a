# AML0000A #


### Objective ###

This is the repository for the frontend of Mileage Calculator which is used to demonstrate Next Generation architecture in September 2016. 


### Components ###

1. The aggregator to use services AML0001A and AML0001B.
2. The frontend codes, including the HTML templates and a JavaScript component which will call the aggregator services of itself.


### Dependencies ###

#### Internal ####

* [AML0001A](https://bitbucket.org/cathaypacific/aml0001a)
* [AML0001B](https://bitbucket.org/cathaypacific/aml0001b)


### Getting Started ###

#### Development Environment ####

* Install [NodeJS 4.3 (and the NPM comes with it)](https://nodejs.org/en/blog/release/v4.3.0/), then:
```
$ npm install
$ npm start
```

#### Configurations ####

*What are the required environment configuration parameters?*

### Deployments ###

The services are continuously integrated at the following URL:

* DEV: http://aml0000a-d0.apps.cpaaws.com