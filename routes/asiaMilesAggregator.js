const express = require('express');
const util = require('util');
const router = express.Router();
const http = require('http');
const Client = require('node-rest-client').Client;
const config = require('../config');

const client = new Client();


/* GET users listing. */
router.get('/', function(req, res, next) {
    res.send('asia_miles_aggregator : respond with a resource');

});

router.post('/getToAirportList/:from', function(req, res, next) {
    var url = util.format("%s/rpc/MetaDataService/getToAirportList/%s", config.mdsUrl, req.params.from);
    console.log("/getToAirportList/"+req.params.from);
    client.post(url, function(data, response) { res.send(data)} );
});

router.post('/getCarrierList/:from/:to', function(req, res, next) {
    var url = util.format("%s/rpc/MetaDataService/getCarrierList/%s/%s", config.mdsUrl, req.params.from, req.params.to);
    console.log(url);
    client.post(url, function(data, response) { res.send(data)});
});

router.post('/calMilelage', function(req, res, next) {
    var url = config.amlUrl + "/rpc/MilelageCalService/calMilelage";
    console.log(url);
    var args = {
        data: req.body,
        headers: { "Content-Type": "application/json" }
    };
    client.post(url, args, function(data, response) { res.send(data) });
});

module.exports = router;
