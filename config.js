module.exports = {
    mdsUrl: process.env.AML0001A_SERVICE_HOST ? 'http://'+process.env.AML0001A_SERVICE_HOST+':'+process.env.AML0001A_SERVICE_PORT : 'http://localhost:18080',
    amlUrl: process.env.AML0001B_SERVICE_HOST ? 'http://'+process.env.AML0001B_SERVICE_HOST+':'+process.env.AML0001B_SERVICE_PORT : 'http://localhost:28080',
};
